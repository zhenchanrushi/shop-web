import { createApp } from 'vue'
import App from './App.vue'
import $ from 'jquery'
import router from './router'

import amazeui from 'amazeui'; //amazeui
// import 'amazeui/dist/css/amazeui.min.css'

// import '@/assets/css/app.css'
const app = createApp(App)
app.use(router)
app.use(amazeui)
app.mount('#app')
