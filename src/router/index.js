import {
	createRouter,
	createWebHistory
} from 'vue-router'

const routes = [{
		path: '/main',
		name: 'main',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import( /* webpackChunkName: "about" */ '@/components/MainFrame.vue'),
		children: [{
				path: '/',
				name: 'home',
				// route level code-splitting
				// this generates a separate chunk (about.[hash].js) for this route
				// which is lazy-loaded when the route is visited.
				component: () => import( /* webpackChunkName: "about" */ '@/views/home.vue')
			},
			{
				path: '/introduction',
				name: 'introduction',
				// route level code-splitting
				// this generates a separate chunk (about.[hash].js) for this route
				// which is lazy-loaded when the route is visited.
				component: () => import( /* webpackChunkName: "about" */ '@/views/introduction.vue')
			},
			{
				path: '/login',
				name: 'login',
				// route level code-splitting
				// this generates a separate chunk (about.[hash].js) for this route
				// which is lazy-loaded when the route is visited.
				component: () => import( /* webpackChunkName: "about" */ '@/views/login.vue')
			},
			{
				path: '/pay',
				name: 'pay',
				component: () => import('@/views/pay.vue')
			},
			{
				path: '/register',
				name: 'register',
				component: () => import('@/views/register.vue')
			},
			{
				path: '/search',
				name: 'search',
				component: () => import('@/views/search.vue')
			},
			{
				path: '/shopcart',
				name: 'shopcart',
				component: () => import('@/views/shopcart.vue')
			},
			{
				path: '/sort',
				name: 'sort',
				component: () => import('@/views/sort.vue')
			},
			{
				path: '/success',
				name: 'success',
				component: () => import('@/views/success.vue')
			}
		]
	},
	{
		path: '/person',
		name: 'person',
		component: () => import('@/components/PersonFrame.vue'),
		children: [{
				path: '/person/index',
				name: 'personIndex',
				component: () => import('@/views/person/index.vue')
			},
			{
				path: '/person/address',
				name: 'address',
				component: () => import('@/views/person/address.vue')
			},
			{
				path: '/person/bill',
				name: 'bill',
				component: () => import('@/views/person/bill.vue')
			},
			{
				path: '/person/billlist',
				name: 'billlist',
				component: () => import('@/views/person/billlist.vue')
			},
			{
				path: '/person/bindphone',
				name: 'bindphone',
				component: () => import('@/views/person/bindphone.vue')
			},
			{
				path: '/person/blog',
				name: 'blog',
				component: () => import('@/views/person/blog.vue')
			},
			{
				path: '/person/bonus',
				name: 'bonus',
				component: () => import('@/views/person/bonus.vue')
			},
			{
				path: '/person/cardlist',
				name: 'cardlist',
				component: () => import('@/views/person/cardlist.vue')
			},
			{
				path: '/person/cardmethod',
				name: 'cardmethod',
				component: () => import('@/views/person/cardmethod.vue')
			},
			{
				path: '/person/change',
				name: 'change',
				component: () => import('@/views/person/change.vue')
			},
			{
				path: '/person/collection',
				name: 'collection',
				component: () => import('@/views/person/collection.vue')
			},
			{
				path: '/person/comment',
				name: 'comment',
				component: () => import('@/views/person/comment.vue')
			},
			{
				path: '/person/commentlist',
				name: 'commentlist',
				component: () => import('@/views/person/commentlist.vue')
			},
			{
				path: '/person/consultation',
				name: 'consultation',
				component: () => import('@/views/person/consultation.vue')
			},
			{
				path: '/person/coupon',
				name: 'coupon',
				component: () => import('@/views/person/coupon.vue')
			},
			{
				path: '/person/email',
				name: 'email',
				component: () => import('@/views/person/email.vue')
			},
			{
				path: '/person/foot',
				name: 'foot',
				component: () => import('@/views/person/foot.vue')
			},
			{
				path: '/person/idcard',
				name: 'idcard',
				component: () => import('@/views/person/idcard.vue')
			},
			{
				path: '/person/information',
				name: 'information',
				component: () => import('@/views/person/information.vue')
			},
			{
				path: '/person/logistics',
				name: 'logistics',
				component: () => import('@/views/person/logistics.vue')
			},
			{
				path: '/person/news',
				name: 'news',
				component: () => import('@/views/person/news.vue')
			},
			{
				path: '/person/order',
				name: 'order',
				component: () => import('@/views/person/order.vue')
			},
			{
				path: '/person/orderinfo',
				name: 'orderinfo',
				component: () => import('@/views/person/orderinfo.vue')
			},
			{
				path: '/person/password',
				name: 'password',
				component: () => import('@/views/person/password.vue')
			},
			{
				path: '/person/pointnew',
				name: 'pointnew',
				component: () => import('@/views/person/pointnew.vue')
			},
			{
				path: '/person/points',
				name: 'points',
				component: () => import('@/views/person/points.vue')
			},
			{
				path: '/person/question',
				name: 'question',
				component: () => import('@/views/person/question.vue')
			},
			{
				path: '/person/record',
				name: 'record',
				component: () => import('@/views/person/record.vue')
			},
			{
				path: '/person/refund',
				name: 'refund',
				component: () => import('@/views/person/refund.vue')
			},
			{
				path: '/person/safety',
				name: 'safety',
				component: () => import('@/views/person/safety.vue')
			},
			{
				path: '/person/setpay',
				name: 'setpay',
				component: () => import('@/views/person/setpay.vue')
			},
			{
				path: '/person/suggest',
				name: 'suggest',
				component: () => import('@/views/person/suggest.vue')
			},
			{
				path: '/person/wallet',
				name: 'wallet',
				component: () => import('@/views/person/wallet.vue')
			},
			{
				path: '/person/walletlist',
				name: 'walletlist',
				component: () => import('@/views/person/walletlist.vue')
			}
		]
	}
]

const routerHistory = createWebHistory('/shop/')
const router = createRouter({
	history: routerHistory,
	routes
})

export default router
