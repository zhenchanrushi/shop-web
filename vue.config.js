const webpack = require('webpack')
module.exports = {
	lintOnSave: false,
	publicPath: '/shop/',
	devServer: {
		// host: '127.0.0.1',
		port: 8080,
		https: false,
		disableHostCheck: true,
		//open: true, //默认是否打开浏览器
		proxy: {
			'/api': {
				target: 'http://iso8859-n.vicp.cc',
				ws: true,
				changeOrigin: true,
				// pathRewrite: {
				// 	'^/api': ''
				// }
			}
		}
	},
	configureWebpack: {
		plugins: [
			new webpack.ProvidePlugin({
				$: "jquery",
				jQuery: "jquery",
				"windows.jQuery": "jquery"
			})
		]
	},
	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				args[0].title = '商城'
				return args
			})
	}
}
